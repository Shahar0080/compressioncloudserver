package dao;

import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   29/11/2021
 * Purpose: A interface to separate low-level data accessing API/operations from high-level business logic
 **/
public interface IDao<N extends Number, T> {

    Map<N, T> getAll();

    Boolean saveAll(Map<N, T> all);

    Boolean add(T t);

    Boolean remove(T t);

    Boolean removeById(N id);

    T getById(N id);

    Boolean hasItem(T t);

    Boolean hasKey(N n);

}
