package dao;

import com.google.gson.reflect.TypeToken;
import compression.ServerDriver;
import io.FileManager;
import json.JsonManager;
import objects.fromclient.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   30/11/2021
 * Purpose: A class to get people data from the Database (In our case - a file)
 **/
public class PeopleDao implements IDao<Long, Person> {
    private static PeopleDao instance = null;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final FileManager fileManager = FileManager.getInstance();
    private final JsonManager jsonManager = JsonManager.getInstance();

    private final String PEOPLE_FILE_PATH = "people.json";
    private final String FULL_PATH = ServerDriver.DATA_FOLDER_PATH + PEOPLE_FILE_PATH;


    private PeopleDao() {
        File dir = fileManager.CreateFolder(ServerDriver.DATA_FOLDER_PATH, true);
        fileManager.createFile(dir, PEOPLE_FILE_PATH);
        updatePersonCount();
    }

    public static PeopleDao getInstance() {
        if (instance == null) {
            instance = new PeopleDao();
        }
        return instance;
    }

    /**
     * A function to get the people map
     *
     * @return - the people map
     */
    @Override
    public Map<Long, Person> getAll() {
        logger.info("Trying to read all people from JSON");
        try {
            String fileData = fileManager.readData(FULL_PATH);
            if (fileData.equals("[]") || fileData.isEmpty()) return new HashMap<>();
            Type type = new TypeToken<Map<Long, Person>>() {
            }.getType();
            logger.info("Successfully read json file. - File path: " + FULL_PATH);
            return jsonManager.fromJson(fileData, type);
        } catch (Exception e) {
            logger.error("Error reading json file! filePath: " + FULL_PATH, e);
            return new HashMap<>();
        }
    }

    /**
     * A function to save the people map to JSON
     *
     * @param people - the people to save
     * @return true if succeeded, else, false
     */
    @Override
    public Boolean saveAll(Map<Long, Person> people) {
        logger.info("Trying to save all people to JSON");
        Type type = new TypeToken<Map<Long, Person>>() {
        }.getType();
        String json = jsonManager.toJson(people, type);
        boolean isSaveSuccess = fileManager.writeData(json, FULL_PATH);
        updatePersonCount();
        if (isSaveSuccess) {
            logger.info("Successfully saved given json to file. - File path: " + FULL_PATH);
        } else {
            logger.error("Error saving json to file! - File path: " + FULL_PATH);
        }
        return isSaveSuccess;
    }

    /**
     * A function to add a new person to the people map
     *
     * @param newPerson - the new person
     * @return - true if added, else, false
     */
    @Override
    public Boolean add(Person newPerson) {
        logger.info("Starting to add person to the people map. user name: "
                + newPerson.getUserNameAndPassword().getUserName());
        Map<Long, Person> people = getAll();
        people.put(newPerson.getId(), newPerson);
        return saveAll(people);
    }

    /**
     * A function to remove a person from the people map.
     *
     * @param person - the person to remove
     * @return - true if succeeded, else, false
     */
    @Override
    public Boolean remove(Person person) {
        logger.info("Starting to remove person from the people map. Person id: " + person.getId());
        Map<Long, Person> people = getAll();

        if (!hasItem(person)) {
            logger.error("Error removing person from the people map. Person id: " + person.getId() + ", couldn't find person in map!");
            return false;
        }
        if (people.remove(person.getId()) == null) {
            logger.error("Error removing person from the people map. Person id: " + person.getId() + ", couldn't remove person from map!");
            return false;
        }
        logger.info("Removed person from the people map. Person id: " + person.getId());
        return saveAll(people);
    }

    /**
     * A function to remove a given person from the people map using it's id
     *
     * @param id - the persons' id
     * @return - true if succeeded, else, false
     */
    @Override
    public Boolean removeById(Long id) {
        logger.info("Trying to remove person with id: " + id);
        Map<Long, Person> people = getAll();

        if (!hasKey(id)) {
            logger.error("Error removing person with id: " + id + ", id not found!");
            return false;
        }

        if (people.remove(id) == null) {
            logger.error("Error removing person with id: " + id + ", couldn't remove id from map!");
            return false;
        }
        return saveAll(people);
    }

    /**
     * A function to get a person by id from the people map
     *
     * @param id - the persons' id
     * @return the person if found, else, null
     */
    @Override
    public Person getById(Long id) {
        logger.info("Returning a person by given id: " + id);
        Map<Long, Person> people = getAll();

        if (people.containsKey(id)) {
            logger.info("Found person with given id: " + id + ", returning it now..");
            return people.get(id);
        }
        logger.error("Could not find person with given id: " + id);
        return null;
    }

    /**
     * A function to get a person by username from the people map
     *
     * @param userName - the given username
     * @return - the person if found, else, null
     */
    public Person getByUsername(String userName) {
        logger.info("Trying to find person with user name: " + userName);
        Map<Long, Person> people = getAll();

        for (Map.Entry<Long, Person> person : people.entrySet()) {
            if (person.getValue().getUserNameAndPassword().getUserName().equalsIgnoreCase(userName)) {
                logger.info("Found user with user name: " + userName);
                return person.getValue();
            }
        }
        logger.warn("Could not find user with user name: " + userName);
        return null;
    }

    /**
     * A function to update the person count.
     * Used when loading a file, in-order to prevent duplicate IDs we set the count as the last persons' id + 1
     */
    public void updatePersonCount() {
        logger.info("Updating the people count now");
        Map<Long, Person> people = getAll();

        long max = people.values().stream()
                .mapToLong(Person::getId)
                .max()
                .orElse(-1);

        Person.setCount(max + 1);
    }

    /**
     * A function to check if a given person exists in the person-map
     *
     * @param person - the person
     * @return true if exists, else, false
     */
    @Override
    public Boolean hasItem(Person person) {
        Map<Long, Person> people = getAll();
        return people.containsValue(person);
    }

    /**
     * A function to check if a given persons' id exists in the person-map
     *
     * @param key - the persons' id
     * @return true if exists, else, false
     */
    @Override
    public Boolean hasKey(Long key) {
        Map<Long, Person> people = getAll();
        return people.containsKey(key);
    }
}
