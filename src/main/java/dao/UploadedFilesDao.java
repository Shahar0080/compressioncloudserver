package dao;

import com.google.gson.reflect.TypeToken;
import compression.ServerDriver;
import io.FileManager;
import json.JsonManager;
import objects.UploadedFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   30/11/2021
 * Purpose: A class to get uploaded-files data from the Database (In our case - a file)
 **/
public class UploadedFilesDao implements IDao<Long, UploadedFile> {
    private static UploadedFilesDao instance = null;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final JsonManager jsonManager = JsonManager.getInstance();
    private final FileManager fileManager = FileManager.getInstance();
    private final String UPLOADED_FILES_FILE_PATH = "uploadedFiles.json";
    private final String FULL_PATH = ServerDriver.DATA_FOLDER_PATH + UPLOADED_FILES_FILE_PATH;

    private UploadedFilesDao() {
        File dataDir = fileManager.CreateFolder(ServerDriver.DATA_FOLDER_PATH, true);
        fileManager.createFile(dataDir, UPLOADED_FILES_FILE_PATH);

        fileManager.CreateFolder(ServerDriver.FILES_FOLDER_PATH, true);

        updateUploadedFilesCount();
    }

    public static UploadedFilesDao getInstance() {
        if (instance == null) {
            instance = new UploadedFilesDao();
        }
        return instance;
    }


    /**
     * A function to get a map of all the uploaded files
     *
     * @return - a map of all the uploaded files
     */
    @Override
    public Map<Long, UploadedFile> getAll() {
        logger.info("Trying to load all uploaded files from JSON");
        try {
            String fileData = fileManager.readData(ServerDriver.DATA_FOLDER_PATH + UPLOADED_FILES_FILE_PATH);
            if (fileData.equals("[]") || fileData.isEmpty()) return new HashMap<>();
            Type type = new TypeToken<Map<Long, UploadedFile>>() {
            }.getType();
            logger.info("Successfully read json file. - File path: " + UPLOADED_FILES_FILE_PATH);
            return jsonManager.fromJson(fileData, type);
        } catch (Exception e) {
            logger.error("Error reading json file! filePath: " + UPLOADED_FILES_FILE_PATH, e);
            return new HashMap<>();
        }
    }

    /**
     * A function to save the uploaded files map to JSON
     *
     * @param uploadedFiles - the uploaded files to save
     * @return true if succeeded, else, false
     */
    @Override
    public Boolean saveAll(Map<Long, UploadedFile> uploadedFiles) {
        logger.info("Trying to save all uploaded files to JSON");
        Type type = new TypeToken<Map<Long, UploadedFile>>() {
        }.getType();
        String json = jsonManager.toJson(uploadedFiles, type);
        boolean isSaveSuccess = fileManager.writeData(json, FULL_PATH);
        updateUploadedFilesCount();

        if (isSaveSuccess) {
            logger.info("Successfully saved given json to file. - File path: " + UPLOADED_FILES_FILE_PATH);
            return true;
        } else {
            logger.error("Error saving json to file! - File path: " + UPLOADED_FILES_FILE_PATH);
            return false;
        }
    }

    /**
     * A function to add a file to the uploaded map.
     *
     * @param newUploadedFile - the file to add
     * @return - true if succeeded, else, false
     */
    @Override
    public Boolean add(UploadedFile newUploadedFile) {
        logger.info("Starting to add file to the uploadedFiles map. file name: " + newUploadedFile.getFileName());
        Map<Long, UploadedFile> uploadedFiles = getAll();
        uploadedFiles.put(newUploadedFile.getId(), newUploadedFile);

        return saveAll(uploadedFiles);
    }

    /**
     * A function to remove a file from the uploaded map.
     *
     * @param uploadedFile - the file to remove
     * @return - true if succeeded, else, false
     */
    @Override
    public Boolean remove(UploadedFile uploadedFile) {
        logger.info("Starting to remove file from the uploadedFiles map. File id: " + uploadedFile.getId());
        Map<Long, UploadedFile> uploadedFiles = getAll();

        if (!hasItem(uploadedFile)) {
            logger.error("Error removing uploaded file from the uploadedFiles map with id: " + uploadedFile.getId() + ", id not found!");
            return false;
        }

        if (!deleteFile(uploadedFile.getSavedFileName())) {
            logger.error("Error removing uploaded file from the uploadedFiles map with id: " + uploadedFile.getId() + ", couldn't delete file!");
            return false;
        }

        if (uploadedFiles.remove(uploadedFile.getId()) == null) {
            logger.error("Error removing uploaded file from the uploadedFiles map with id: " + uploadedFile.getId() + ", couldn't remove id from map");
            return false;
        }

        return saveAll(uploadedFiles);
    }

    /**
     * A function to remove a file from the uploaded map by id.
     *
     * @param id - the file id to remove
     * @return - true if succeeded, else, false
     */
    @Override
    public Boolean removeById(Long id) {
        logger.info("Starting to remove uploaded file from the uploadedFiles map by id. File id: " + id);
        Map<Long, UploadedFile> uploadedFiles = getAll();

        if (!hasKey(id)) {
            logger.error("Error removing uploaded file from the uploadedFiles map with id: " + id + ", id not found!");
            return false;
        }

        if (!deleteFile(getById(id).getSavedFileName())) {
            logger.error("Error removing uploaded file from the uploadedFiles map with id: " + id + ", couldn't delete file!");
            return false;
        }

        if (uploadedFiles.remove(id) == null) {
            logger.error("Error removing uploaded file from the uploadedFiles map with id: " + id + ", couldn't remove id from map");
            return false;
        }

        return saveAll(uploadedFiles);
    }

    /**
     * A function to get a uploaded file by it's id
     *
     * @param id - the given files id
     * @return - the file if found, else, null
     */
    @Override
    public UploadedFile getById(Long id) {
        logger.info("Returning a uploaded file by given id: " + id);
        Map<Long, UploadedFile> uploadedFiles = getAll();
        if (hasKey(id)) {
            logger.info("Found uploaded file with given id: " + id + ", returning it now..");
            return uploadedFiles.get(id);
        }
        logger.error("Could not find uploaded file with given id: " + id);
        return null;
    }

    /**
     * A function to update the person count.
     * Used when loading a file, in-order to prevent duplicate IDs we set the count as the last persons' id + 1
     */
    public void updateUploadedFilesCount() {
        logger.info("Updating the uploaded files count now");
        Map<Long, UploadedFile> uploadedFiles = getAll();

        long max = uploadedFiles.values().stream()
                .mapToLong(UploadedFile::getId)
                .max()
                .orElse(-1);

        UploadedFile.setCount(max + 1);
    }

    /**
     * A function to check if a given uploaded-file exists in the uploaded-files-map
     *
     * @param uploadedFile - the uploaded file
     * @return true if exists, else, false
     */
    @Override
    public Boolean hasItem(UploadedFile uploadedFile) {
        Map<Long, UploadedFile> uploadedFiles = getAll();
        return uploadedFiles.containsValue(uploadedFile);
    }

    /**
     * A function to check if a given file id exists in the uploaded-files-map
     *
     * @param key - the id
     * @return true if exists, else, false
     */
    @Override
    public Boolean hasKey(Long key) {
        Map<Long, UploadedFile> uploadedFiles = getAll();
        return uploadedFiles.containsKey(key);
    }

    /**
     * A function to delete a file using its' filename
     *
     * @param fileName - the given file name
     * @return true if succeeded, else, false
     */
    private boolean deleteFile(String fileName) {
        return fileManager.deleteFile(ServerDriver.FILES_FOLDER_PATH + fileName);
    }
}
