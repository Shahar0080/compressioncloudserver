package server;

import json.JsonManager;
import objects.tcp.Request;
import objects.tcp.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Author: Shahar Azar
 * Date:   25/12/2021
 * Purpose: A class that is listening on a designated port.
 * The class acts as a rest-side for our server so it can handle
 * and perform various operations.
 **/
public class PureServer implements Runnable {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final JsonManager jsonManager = JsonManager.getInstance();
    private final PureRequestHandler pureRequestHandler = PureRequestHandler.getInstance();
    private final int MAX_TRIES = 3;
    private final int port;


    public PureServer(int port) {
        this.port = port;
    }

    public void run() {
        AtomicInteger tries = new AtomicInteger(0);
        logger.info("Starting pure server..");
        while (tries.intValue() < MAX_TRIES) {
            try {
                ServerSocket serverSocket = new ServerSocket(this.port);
                logger.info("Server socket successfully created on port: " + this.port);
                while (true) {
                    Socket s = serverSocket.accept(); //establishes connection
                    logger.info("Packet received");
                    new Thread(() -> {
                        try {
                            // read data from client
                            InputStreamReader isr = new InputStreamReader(s.getInputStream());
                            BufferedReader br = new BufferedReader(isr);
                            String cmd = br.readLine();
                            Request request = jsonManager.fromJson(cmd, Request.class);
                            logger.info("A request was received. Request: " + request.toString());

                            // handle request
                            Response response = pureRequestHandler.handleRequest(request);

                            //send the result back to the client as json
                            logger.info("Sending the response back. Response: " + response.toString());
                            PrintWriter pr = new PrintWriter(s.getOutputStream());
                            pr.println(jsonManager.toJson(response, Response.class));
                            pr.flush();
                            s.close();
                        } catch (IOException ioException) {
                            logger.error("Error while handling request", ioException);
                        }
                    }).start();
                }
            } catch (Exception e) {
                tries.incrementAndGet();
                logger.error("General server error", e);
            }
        }
    }
}
