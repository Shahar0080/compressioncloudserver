package server;

import algorithms.AlgorithmManager;
import com.google.gson.reflect.TypeToken;
import compression.ServerDriver;
import constants.ServerPaths;
import controller.FilesToDownloadController;
import controller.PeopleController;
import controller.UploadedFilesController;
import io.FileManager;
import json.JsonManager;
import objects.CompressionResult;
import objects.FileToDownload;
import objects.FileToUpload;
import objects.UploadedFile;
import objects.fromclient.Person;
import objects.fromclient.UserNameAndPassword;
import objects.tcp.Request;
import objects.tcp.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   03/01/2022
 * Purpose: A class to to handle client requests
 **/
@SuppressWarnings("DuplicatedCode")
public class PureRequestHandler {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final UploadedFilesController uploadedFilesController = UploadedFilesController.getInstance();
    private final PeopleController peopleController = PeopleController.getInstance();
    private final FilesToDownloadController filesToDownloadController = FilesToDownloadController.getInstance();
    private final JsonManager jsonManager = JsonManager.getInstance();
    private final AlgorithmManager algorithmManager = AlgorithmManager.getInstance();
    private final FileManager fileManager = FileManager.getInstance();
    private static PureRequestHandler instance = null;

    private PureRequestHandler() {

    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static PureRequestHandler getInstance() {
        if (instance == null) {
            instance = new PureRequestHandler();
        }
        return instance;
    }

    /**
     * A function to get a response for each requests
     *
     * @param request - the request
     * @return - the response
     */
    public Response handleRequest(Request request) {
        switch (request.getAction()) {
            case ServerPaths.IS_USERNAME_AVAILABLE:
                return isUsernameAvailable(request.getData());
            case ServerPaths.USER_LOGIN:
                return performUserLogin(request.getData());
            case ServerPaths.USER_REGISTER:
                return performUserSignUp(request.getData());
            case ServerPaths.FILE_UPLOAD:
                return uploadFile(request.getData());
            case ServerPaths.FILE_DOWNLOAD:
                return downloadFile(request.getData());
            case ServerPaths.FILE_DELETION:
                return deleteFile(request.getData());
            case ServerPaths.GET_ALL_FILES:
                return getAllFiles();
        }
        return null;
    }

    /**
     * A function to check if a username is available
     *
     * @param username - the username
     * @return - the response containing the result
     */
    private Response isUsernameAvailable(String username) {
        logger.info("Client made a request to check if a username is available");
        String userName = jsonManager.fromJson(username, String.class);
        boolean isAvailable = peopleController.isUsernameAvailable(userName);
        logger.info("Username " + userName + " availability status is: " + isAvailable);
        return new Response(jsonManager.toJson(isAvailable, boolean.class));
    }

    /**
     * A function to perform user login
     *
     * @param usernameAndPassword - the username and password
     * @return - the response containing the result
     */
    private Response performUserLogin(String usernameAndPassword) {
        logger.info("Client made a request to perform user login ");

        UserNameAndPassword loginInfo = jsonManager.fromJson(usernameAndPassword, UserNameAndPassword.class);
        String userName = loginInfo.getUserName();
        String password = loginInfo.getPassword();

        Person personToLogin = peopleController.getPersonByUserName(userName);

        if (personToLogin == null) {
            logger.info("Received credentials that are not known. Username: " + userName + ", Password: " + password);
            return new Response(null, "Received credentials that are not known. Username: " + userName + ", Password: " + password);
        }

        if (!personToLogin.getUserNameAndPassword().getPassword().equals(password)) {
            logger.info("Received credentials that are incorrect. Username: " + userName + ", Password: " + password);
            return new Response(null, "Received credentials that are incorrect. Username: " + userName + ", Password: " + password);
        }

        logger.info("Username: " + userName + " has signed in.");
        return new Response(jsonManager.toJson(personToLogin, Person.class));
    }

    /**
     * A function to perform user signup
     *
     * @param personData - the person to signup
     * @return - the response containing the result
     */
    private Response performUserSignUp(String personData) {
        logger.info("Client made a request to register a user");
        Person person = jsonManager.fromJson(personData, Person.class);

        Person newPerson = new Person(person);
        boolean isAdded = peopleController.addPerson(newPerson);
        if (!isAdded) {
            peopleController.removePerson(newPerson);
            logger.warn("Error adding "
                    + newPerson.getUserNameAndPassword().getUserName() + " username already exists.");
            return new Response(null, "Error adding "
                    + newPerson.getUserNameAndPassword().getUserName() + " username already exists.");
        }

        logger.info("Successfully registered person with data: " + newPerson.getUserNameAndPassword().getUserName());
        return new Response(jsonManager.toJson(newPerson.getUserNameAndPassword(), UserNameAndPassword.class));
    }

    /**
     * A function upload a file
     *
     * @param fileToUploadData - the file to upload
     * @return - the response containing the result
     */
    private Response uploadFile(String fileToUploadData) {
        logger.info("Client made a request to upload a file");
        FileToUpload fileToUpload = jsonManager.fromJson(fileToUploadData, FileToUpload.class);
        String fileName = fileToUpload.getFileName();
        String data = fileToUpload.getData();
        logger.info("Starting to compress file with name: " + fileName);

        String savedFileName = System.currentTimeMillis() + fileName;
        CompressionResult compressionResult = algorithmManager.encodeData(data);
        boolean isDataSaved = fileManager.writeData(compressionResult.getData(), ServerDriver.FILES_FOLDER_PATH + savedFileName);

        if (!isDataSaved) {
            logger.error("Could not add the file: " + fileName + " due to internal server error");
            return new Response(null, "Could not add the file: " + fileName + " due to internal server error");
        }

        logger.info("Successfully uploaded and compressed file with name: " + fileName +
                ", using algorithm:" + compressionResult.getAlgorithmType());

        String size = String.valueOf(compressionResult.getData().length());

        logger.info("Starting to add the file with name: " + fileName + " to the uploadedFilesMap");
        boolean isAddToMap = uploadedFilesController.addFileToMap(new UploadedFile(fileToUpload.getFileName(),
                savedFileName, compressionResult.getAlgorithmType(), size, fileToUpload.getFileOwner()));

        if (isAddToMap) {
            logger.info("Successfully added the file with the name: " + fileName + " to the uploadedFilesMap");
            return new Response(jsonManager.toJson(true, boolean.class));
        } else {
            logger.error("Could not add the file: " + fileName + " due to internal server error");
            return new Response(jsonManager.toJson(false, boolean.class), "Could not add the file: " + fileName + " due to internal server error");
        }
    }

    /**
     * A function to download a file
     *
     * @param fileIdData - the file-to-download id
     * @return - the response containing the result
     */
    private Response downloadFile(String fileIdData) {
        logger.info("Client made a request to download a file");
        String fileIdAsString = jsonManager.fromJson(fileIdData, String.class);
        long fileId = Long.parseLong(fileIdAsString);

        FileToDownload fileToDownload = filesToDownloadController.getFile(fileId);
        if (fileToDownload == null) {
            logger.error("Error downloading file with id of: " + fileId);
            return new Response(null, "Error downloading file with id of: " + fileId);
        }

        UploadedFile uploadedFile = uploadedFilesController.getUploadedFileById(fileId);

        if (uploadedFile == null) {
            logger.error("Could not find the file with the id: " + fileId + " in the uploaded map");
            return new Response(null, "Could not find the file with the id: " + fileId + " in the uploaded map");
        }

        String fileData = fileManager.readData(ServerDriver.FILES_FOLDER_PATH + uploadedFile.getFileName());

        String compressionResult = algorithmManager.decodeData(fileData, uploadedFile.getAlgorithmType());

        logger.info("Successfully read and decompressed file with the id: " + fileId + ", sending back..");
        return new Response(jsonManager.toJson(compressionResult, String.class));
    }

    /**
     * A function to delete a file
     *
     * @param fileIdData - the file-to-delete id
     * @return - the response containing the result
     */
    private Response deleteFile(String fileIdData) {
        logger.info("Client made a request to delete a file");
        String fileIdAsString = jsonManager.fromJson(fileIdData, String.class);
        long fileId = Long.parseLong(fileIdAsString);

        boolean fileToDownload = uploadedFilesController.removeFileById(fileId);

        if (!fileToDownload) {
            logger.error("Error deleting file with id of: " + fileId);
            return new Response(jsonManager.toJson(false, boolean.class), "Error deleting file with id of: " + fileId);
        }

        logger.info("Successfully deleted file with the id: " + fileId);
        return new Response(jsonManager.toJson(true, boolean.class));
    }

    /**
     * A function that returns all the files
     *
     * @return - the response containing the result
     */
    private Response getAllFiles() {
        logger.info("Client made a request to get all the files");

        Map<Long, FileToDownload> files = filesToDownloadController.getAllFiles();
        Type type = new TypeToken<Map<Long, FileToDownload>>() {
        }.getType();
        return new Response(jsonManager.toJson(files, type));
    }

}
