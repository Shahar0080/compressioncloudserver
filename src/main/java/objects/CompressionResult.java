package objects;

import algorithms.AlgorithmType;

/**
 * Author: Shahar Azar
 * Date:   04/12/2021
 * Purpose: A class to represent a result containing the compressed data nad the algorithm it was compressed with.
 **/
public class CompressionResult {
    String data;
    AlgorithmType algorithmType;

    public CompressionResult(String data, AlgorithmType algorithmType) {
        this.data = data;
        this.algorithmType = algorithmType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public AlgorithmType getAlgorithmType() {
        return algorithmType;
    }

    public void setAlgorithmType(AlgorithmType algorithmType) {
        this.algorithmType = algorithmType;
    }
}
