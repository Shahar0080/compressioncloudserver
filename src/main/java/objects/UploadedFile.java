package objects;

import algorithms.AlgorithmType;
import objects.fromclient.Person;

import java.time.LocalDate;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to represent the file the client is trying to upload
 **/
public class UploadedFile {
    private static long count = 0;

    private long id;
    private String fileName;
    private String savedFileName;
    private AlgorithmType algorithmType;
    private String fileSize;
    private Person fileOwner;
    private LocalDate dateAdded;

    public UploadedFile(String fileName, String savedFileName, AlgorithmType algorithmType, String fileSize, Person fileOwner) {
        this.id = count++;
        this.fileName = fileName;
        this.savedFileName = savedFileName;
        this.algorithmType = algorithmType;
        this.fileOwner = fileOwner;
        this.fileSize = fileSize;
        this.dateAdded = LocalDate.now();
    }

    public static long getCount() {
        return count;
    }

    public static void setCount(long count) {
        UploadedFile.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSavedFileName() {
        return savedFileName;
    }

    public void setSavedFileName(String savedFileName) {
        this.savedFileName = savedFileName;
    }

    public AlgorithmType getAlgorithmType() {
        return algorithmType;
    }

    public void setAlgorithmType(AlgorithmType algorithmType) {
        this.algorithmType = algorithmType;
    }

    public Person getFileOwner() {
        return fileOwner;
    }

    public void setFileOwner(Person fileOwner) {
        this.fileOwner = fileOwner;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public LocalDate getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDate dateAdded) {
        this.dateAdded = dateAdded;
    }

}
