package controller;

import objects.UploadedFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.UploadedFilesService;

import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   27/12/2021
 * Purpose: A wrapper controller class for {@link UploadedFile}
 **/
public class UploadedFilesController {
    private static UploadedFilesController instance = null;
    private final UploadedFilesService uploadedFilesService = UploadedFilesService.getInstance();
    private final Logger logger = LogManager.getLogger(this.getClass());

    private UploadedFilesController() {
    }

    public static UploadedFilesController getInstance() {
        if (instance == null) {
            instance = new UploadedFilesController();
        }
        return instance;
    }

    /**
     * A wrapper function to add a file to the uploaded map.
     *
     * @param fileToUpload - the file to add
     * @return - true if succeeded, else, false
     */
    public boolean addFileToMap(UploadedFile fileToUpload) {
        logger.info("Adding file with name: " + fileToUpload.getFileName());
        return uploadedFilesService.addFileToMap(fileToUpload);
    }

    /**
     * A function to remove a file from the uploaded map by id.
     *
     * @param id - the file id to remove
     * @return - true if succeeded, else, false
     */
    public boolean removeFileById(Long id) {
        logger.info("Trying to remove file from the uploaded map with id: " + id);
        return uploadedFilesService.removeFileById(id);
    }

    /**
     * A wrapper function to get a uploaded file by it's id
     *
     * @param id - the given files id
     * @return - the file if found, else, null
     */
    public UploadedFile getUploadedFileById(long id) {
        logger.info("Returning file with id: " + id);
        return uploadedFilesService.getUploadedFileById(id);
    }

    /**
     * A wrapper function to get a map of all the uploaded files
     *
     * @return - a map of all the uploaded files
     */
    public Map<Long, UploadedFile> getAllFiles() {
        logger.info("Returning all files");
        return uploadedFilesService.getAllFiles();
    }

    @Override
    public String toString() {
        return uploadedFilesService.toString();
    }
}
