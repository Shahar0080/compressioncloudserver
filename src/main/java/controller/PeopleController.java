package controller;

import objects.fromclient.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.PeopleService;

import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   27/12/2021
 * Purpose: A wrapper controller class for {@link Person}
 **/
public class PeopleController {
    private static PeopleController instance = null;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final PeopleService peopleService = PeopleService.getInstance();

    private PeopleController() {
    }

    public static PeopleController getInstance() {
        if (instance == null) {
            instance = new PeopleController();
        }
        return instance;
    }

    /**
     * A wrapper function to add a new person
     *
     * @param newPerson - the new person
     * @return true if succeeded, else, false
     */
    public boolean addPerson(Person newPerson) {
        logger.info("Trying to add person with username: " + newPerson.getUserNameAndPassword().getUserName());
        return peopleService.addPerson(newPerson);
    }

    /**
     * A function to check if a given user name is available
     *
     * @param userName - the user name
     * @return true if available, else, false
     */
    public boolean isUsernameAvailable(String userName) {
        logger.info("Checking if given username is available. Given username: " + userName);
        return peopleService.isUsernameAvailable(userName);
    }

    /**
     * A wrapper function to remove a given person from the people map using a {@link Person}
     *
     * @param person - the given person
     */
    public boolean removePerson(Person person) {
        logger.info("Removing person from people map with the username: "
                + person.getUserNameAndPassword().getUserName());
        return peopleService.removePerson(person);
    }

    /**
     * A wrapper function to remove a given person from the people map using a {@link Person}
     *
     * @param id - the given persons' id
     */
    public boolean removePerson(long id) {
        logger.info("Removing person from the people map with the id: " + id);
        return peopleService.removePerson(id);
    }

    /**
     * A wrapper function to get a person by id from the people map
     *
     * @param id - the persons' id
     * @return the person if found, else, null
     */
    public Person getPersonById(long id) {
        logger.info("Trying to return person with the id of: " + id);
        return peopleService.getPersonById(id);
    }

    /**
     * A wrapper function to get a person by username from the people map
     *
     * @param userName - the given username
     * @return - the person if found, else, null
     */
    public Person getPersonByUserName(String userName) {
        logger.info("Trying to get a person with the user name: " + userName);
        return peopleService.getPersonByUserName(userName);
    }

    /**
     * A wrapper function to get the people map
     *
     * @return - the people map
     */
    public Map<Long, Person> getPeople() {
        logger.info("Returning all people.");
        return peopleService.getPeople();
    }

    @Override
    public String toString() {
        return peopleService.toString();
    }

}
