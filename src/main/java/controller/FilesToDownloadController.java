package controller;

import objects.FileToDownload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import services.FilesToDownloadService;

import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   27/12/2021
 * Purpose: A wrapper controller class for {@link FileToDownload}
 **/
public class FilesToDownloadController {
    private static FilesToDownloadController instance = null;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final FilesToDownloadService filesToDownloadService = FilesToDownloadService.getInstance();

    private FilesToDownloadController() {
    }

    public static FilesToDownloadController getInstance() {
        if (instance == null) {
            instance = new FilesToDownloadController();
        }
        return instance;
    }

    /**
     * A function to get a file using it's id
     *
     * @param id - the id of the file
     * @return {@link FileToDownload} if found, else, null
     */
    public FileToDownload getFile(long id) {
        logger.info("Trying to get a file with the id of: " + id);
        return filesToDownloadService.getFile(id);
    }

    /**
     * A function to get all the files as a {@link FileToDownload} map
     *
     * @return the {@link FileToDownload} map
     */
    public Map<Long, FileToDownload> getAllFiles() {
        logger.info("Starting to return all download-able files");
        return filesToDownloadService.getAllFiles();
    }

}
