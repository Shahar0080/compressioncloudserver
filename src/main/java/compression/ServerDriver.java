package compression;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.PureServer;

import java.io.File;

/**
 * Author: Shahar Azar
 * Date:   08/11/2021
 * Purpose: The main class
 **/
public class ServerDriver {
    private final static Logger logger = LogManager.getLogger(ServerDriver.class);
    public static final String DATA_FOLDER_PATH = "data" + File.separator;
    public static final String FILES_FOLDER_PATH = "uploaded-files" + File.separator;
    private static final int MAX_TRIES = 3;

    public static void main(String[] args) {
        int tries = 1;
        while (tries <= MAX_TRIES) {
            try {
                logger.info("Starting pure server.. - try number: " + tries);
                PureServer pureServer = new PureServer(25421);
                Thread serverThread = new Thread(pureServer);
                serverThread.start();
                serverThread.join();
            } catch (Exception e) {
                tries++;
                logger.error("There was an exception trying to start the pure server..", e);
            }
        }
    }
}
