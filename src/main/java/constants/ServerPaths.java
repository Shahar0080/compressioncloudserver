package constants;

/**
 * Author: Shahar Azar
 * Date:   03/01/2022
 * Purpose: A class that has all the server paths
 **/
public class ServerPaths {
    public final static String USER_REGISTER = "/user-register/";
    public final static String IS_USERNAME_AVAILABLE = "/is-username-available/";
    public final static String USER_LOGIN = "/user-login/";

    public final static String FILE_UPLOAD = "/upload-file/";
    public final static String FILE_DOWNLOAD = "/download-file/";
    public final static String FILE_DELETION = "/delete-file/";
    public final static String GET_ALL_FILES = "/get-all-files/";
}
