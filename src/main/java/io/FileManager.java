package io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Semaphore;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to read/write from files
 **/
public class FileManager {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private static FileManager instance = null;
    private static final Semaphore fileSemaphore = new Semaphore(1);

    private FileManager() {

    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static FileManager getInstance() {
        if (instance == null) {
            instance = new FileManager();
        }
        return instance;
    }

    /**
     * A function to read data from a given file path
     *
     * @param filePath - the given file path
     * @return - the file data if successful, else, an empty String
     */
    public String readData(String filePath) {
        logger.info("Starting to read data from file. file path: " + filePath);
        String fileContent = "";
        try {
            fileSemaphore.acquire();
            fileContent = new String(Files.readAllBytes(Paths.get(filePath)));
            logger.info("Successfully read data from file. file path: " + filePath);
        } catch (Exception e) {
            logger.error("Error reading data from file! - Given file path: " + filePath, e);
        } finally {
            fileSemaphore.release();
        }
        return fileContent;
    }

    /**
     * A function to write given data to a given file
     *
     * @param data     - the given data
     * @param filePath - the path to the given file
     * @return - true is succeeded, else, false
     */
    public boolean writeData(String data, String filePath) {
        logger.info("Starting to write data from to a file. file path: " + filePath);
        FileWriter fileWriter;
        try {
            fileSemaphore.acquire();
            fileWriter = new FileWriter(filePath);
            fileWriter.write(data);
            fileWriter.close();
            logger.info("Successfully read data from a file. file path: " + filePath);
            return true;
        } catch (Exception e) {
            logger.error("Error writing data to file! - Given file path: " + filePath, e);
        } finally {
            fileSemaphore.release();
        }
        return false;
    }

    /**
     * A wrapper function to create a folder with a default isSystemExit parameter set as false
     *
     * @param folderName - the folder name
     * @return - A {@link File} representing the directory
     */
    public File CreateFolder(String folderName) {
        return CreateFolder(folderName, false);
    }

    /**
     * A function to create a folder with an option to perform system exit on failure
     *
     * @param folderName   - the folder name
     * @param isSystemExit - true if to perform system exit on faliure
     * @return - A {@link File} representing the directory
     */
    public File CreateFolder(String folderName, boolean isSystemExit) {
        File dir = null;
        try {
            fileSemaphore.acquire();
            dir = new File("." + File.separator + folderName + File.separator);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    logger.error("Error creating " + folderName + " folder!");
                    if (isSystemExit) {
                        logger.info("Performing system exit..");
                        System.exit(0);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error creating folder! - Given folder name: " + folderName, e);
        } finally {
            fileSemaphore.release();
        }
        return dir;
    }

    /**
     * A wrapper function to create a file in a given directory with a default isSystemExit parameter set as false
     *
     * @param directory - the given directory, represented as a {@link File}
     * @param fileName  - the given file name
     */
    public void createFile(File directory, String fileName) {
        createFile(directory, fileName, false);
    }

    /**
     * A function to create a file in a given directory with an option to perform system exit on failure
     *
     * @param directory    - the given directory, represented as a {@link File}
     * @param fileName     - the given file name
     * @param isSystemExit - true if to perform system exit on failure
     */
    public void createFile(File directory, String fileName, boolean isSystemExit) {
        boolean isSuccess = false;
        try {
            fileSemaphore.acquire();
            File newFile = new File(directory.getPath() + File.separator + fileName);
            isSuccess = newFile.createNewFile();
        } catch (Exception e) {
            logger.error("Error creating " + fileName + " file!", e);
        } finally {
            fileSemaphore.release();
        }
        if (!isSuccess) {
            logger.error("Error creating " + fileName + " file!");
            if (isSystemExit) {
                logger.info("Performing system exit..");
                System.exit(0);
            }
        }
    }

    /**
     * A function to delete a file
     *
     * @param fileName - the given file name
     * @return - true if succeeded, else, false
     */
    public boolean deleteFile(String fileName) {
        boolean isSuccess = false;
        try {
            fileSemaphore.acquire();
            File fileToDelete = new File(fileName);
            isSuccess = fileToDelete.delete();
        } catch (Exception e) {
            logger.error("Error deleting file! - Given file name: " + fileName, e);
        } finally {
            fileSemaphore.release();
        }
        return isSuccess;
    }
}