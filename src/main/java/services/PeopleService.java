package services;


import com.google.gson.reflect.TypeToken;
import dao.PeopleDao;
import json.JsonManager;
import objects.fromclient.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to perform operations on people
 **/
public class PeopleService {
    private static PeopleService instance = null;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final PeopleDao peopleDao = PeopleDao.getInstance();
    private final JsonManager jsonManager = JsonManager.getInstance();

    private PeopleService() {
    }

    public static PeopleService getInstance() {
        if (instance == null) {
            instance = new PeopleService();
        }
        return instance;
    }

    /**
     * A wrapper function to add a new person
     *
     * @param newPerson - the new person
     * @return true if succeeded, else, false
     */
    public boolean addPerson(Person newPerson) {
        logger.info("Trying to add person with username: " + newPerson.getUserNameAndPassword().getUserName());
        if (!validateNewPerson(newPerson)) {
            logger.warn("Given person did not pass validation..");
            return false;
        }
        return peopleDao.add(newPerson);
    }

    /**
     * A wrapper function to add a new person to the people map (after validating)
     *
     * @param newPerson - the new person
     * @return - true if valid, else, false
     */
    private boolean validateNewPerson(Person newPerson) {
        logger.info("Checking if given person is valid");
        boolean isValid = true;

        // Check if user name already exists
        isValid = isUsernameAvailable(newPerson.getUserNameAndPassword().getUserName());
        logger.info("Username Available validity status: " + isValid);

        return isValid;
    }

    /**
     * A function to check if a given user name is available
     *
     * @param userName - the user name
     * @return true if available, else, false
     */
    public boolean isUsernameAvailable(String userName) {
        logger.info("Checking if given username is available. Given username: " + userName);
        return (peopleDao.getByUsername(userName) == null);
    }

    /**
     * A wrapper function to remove a given person from the people map using a {@link Person}
     *
     * @param person - the given person
     */
    public boolean removePerson(Person person) {
        logger.info("Removing person from people map with the username: "
                + person.getUserNameAndPassword().getUserName());
        return peopleDao.remove(person);
    }

    /**
     * A wrapper function to remove a given person from the people map using a {@link Person}
     *
     * @param id - the given persons' id
     */
    public boolean removePerson(long id) {
        logger.info("Removing person from the people map with the id: " + id);
        return peopleDao.removeById(id);
    }

    /**
     * A wrapper function to get a person by id from the people map
     *
     * @param id - the persons' id
     * @return the person if found, else, null
     */
    public Person getPersonById(long id) {
        logger.info("Trying to return person with the id of: " + id);
        return peopleDao.getById(id);
    }

    /**
     * A wrapper function to get a person by username from the people map
     *
     * @param userName - the given username
     * @return - the person if found, else, null
     */
    public Person getPersonByUserName(String userName) {
        logger.info("Trying to get a person with the user name: " + userName);
        return peopleDao.getByUsername(userName);
    }

    /**
     * A wrapper function to get the people map
     *
     * @return - the people map
     */
    public Map<Long, Person> getPeople() {
        logger.info("Returning all people.");
        return peopleDao.getAll();
    }

    @Override
    public String toString() {
        Type type = new TypeToken<Map<Long, Person>>() {
        }.getType();
        return jsonManager.toJson(peopleDao.getAll(), type);
    }
}
