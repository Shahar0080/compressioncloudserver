package services;

import com.google.gson.reflect.TypeToken;
import dao.UploadedFilesDao;
import json.JsonManager;
import objects.UploadedFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to perform operations regarding the uploaded-files
 **/
public class UploadedFilesService {
    private static UploadedFilesService instance = null;
    private final UploadedFilesDao uploadedFilesDao = UploadedFilesDao.getInstance();
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final JsonManager jsonManager = JsonManager.getInstance();

    private UploadedFilesService() {
    }

    public static UploadedFilesService getInstance() {
        if (instance == null) {
            instance = new UploadedFilesService();
        }
        return instance;
    }

    /**
     * A wrapper function to add a file to the uploaded map.
     *
     * @param fileToUpload - the file to add
     * @return - true if succeeded, else, false
     */
    public boolean addFileToMap(UploadedFile fileToUpload) {
        logger.info("Adding file with name: " + fileToUpload.getFileName());
        return uploadedFilesDao.add(fileToUpload);
    }

    /**
     * A function to remove a file from the uploaded map by id.
     *
     * @param id - the file id to remove
     * @return - true if succeeded, else, false
     */
    public boolean removeFileById(Long id) {
        logger.info("Trying to remove file from the uploaded map with id: " + id);
        return uploadedFilesDao.removeById(id);
    }

    /**
     * A wrapper function to get a uploaded file by it's id
     *
     * @param id - the given files id
     * @return - the file if found, else, null
     */
    public UploadedFile getUploadedFileById(long id) {
        logger.info("Returning file with id: " + id);
        return uploadedFilesDao.getById(id);
    }

    /**
     * A wrapper function to get a map of all the uploaded files
     *
     * @return - a map of all the uploaded files
     */
    public Map<Long, UploadedFile> getAllFiles() {
        logger.info("Returning all files");
        return uploadedFilesDao.getAll();
    }

    @Override
    public String toString() {
        Type type = new TypeToken<Map<Long, UploadedFile>>() {
        }.getType();
        return jsonManager.toJson(uploadedFilesDao.getAll(), type);
    }
}
