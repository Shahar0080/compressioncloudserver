package services;

import objects.FileToDownload;
import objects.UploadedFile;
import objects.fromclient.FileType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   20/11/2021
 * Purpose: A class to perform operations on files-to-download
 **/
public class FilesToDownloadService {
    private static FilesToDownloadService instance = null;
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final UploadedFilesService uploadedFilesService = UploadedFilesService.getInstance();

    private FilesToDownloadService() {
    }

    public static FilesToDownloadService getInstance() {
        if (instance == null) {
            instance = new FilesToDownloadService();
        }
        return instance;
    }

    /**
     * A function to get a file using it's id
     *
     * @param id - the id of the file
     * @return {@link FileToDownload} if found, else, null
     */
    public FileToDownload getFile(long id) {
        logger.info("Trying to get a file with the id of: " + id);
        UploadedFile file = uploadedFilesService.getUploadedFileById(id);

        if (file != null) {
            logger.info("Found file with the id of: " + id);
            return new FileToDownload(file.getId(), file.getFileName(), file.getDateAdded(),
                    getFileType(file.getFileName()), file.getFileSize(), file.getFileOwner(),
                    file.getAlgorithmType().name());
        }
        logger.error("Could not find file with the id of: " + id);
        return null;
    }

    /**
     * A function to get all the files as a {@link FileToDownload} map
     *
     * @return the {@link FileToDownload} map
     */
    public Map<Long, FileToDownload> getAllFiles() {
        logger.info("Starting to return all download-able files");
        Map<Long, UploadedFile> files = uploadedFilesService.getAllFiles();
        Map<Long, FileToDownload> filesToDownload = new HashMap<>();

        files.forEach((id, file) -> filesToDownload.put(file.getId(), new FileToDownload(file.getId(), file.getFileName(), file.getDateAdded(),
                getFileType(file.getFileName()), file.getFileSize(), file.getFileOwner(), file.getAlgorithmType().name())));
        logger.info("Returning a map containing all the files to download with the size of: " + filesToDownload.size());
        return filesToDownload;
    }

    /**
     * A function to get the file type of each file
     *
     * @param fileName - the file name
     * @return - the designated {@link FileType}, if not found - the default is TEXT
     */
    private FileType getFileType(String fileName) {
        logger.info("Trying to get file type of file: " + fileName);
        String extension = getExtension(fileName);
        logger.info("The extension is: " + extension);
        switch (extension) {
            case "txt":
                return FileType.TEXT;
            case "json":
                return FileType.JSON;
            case "xml":
                return FileType.XML;
            case "log":
                return FileType.LOG;
            default:
                return FileType.TEXT;
        }
    }

    /**
     * A function to get the extension of the given file
     *
     * @param fileName - the given file
     * @return A String containing the file extension
     */
    private String getExtension(String fileName) {
        logger.debug("Trying to parse extension of file with name of: " + fileName);
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0 && i < fileName.length() - 1) //if the name is not empty
            return fileName.substring(i + 1).toLowerCase();

        logger.info("Successfully parsed extension of file with name of: " + fileName + ", extension: " + extension);
        return extension;
    }
}
