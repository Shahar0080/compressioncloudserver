package algorithms;


import algorithms.gzip.GZipAlgorithm;
import algorithms.lzw.LZWAlgorithm;
import objects.CompressionResult;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to manage all of the algorithms we have
 **/
public class AlgorithmManager {

    private static AlgorithmManager instance = null;
    private LZWAlgorithm lzwAlgorithm;
    private GZipAlgorithm gzipAlgorithm;

    private AlgorithmManager() {
        lzwAlgorithm = LZWAlgorithm.getInstance();
        gzipAlgorithm = GZipAlgorithm.getInstance();
    }

    public static AlgorithmManager getInstance() {
        if (instance == null) {
            instance = new AlgorithmManager();
        }
        return instance;
    }

    /**
     * A function to encode a given data using the most efficient algorithm
     *
     * @param data - the data to encode
     * @return - the compression result
     */
    public CompressionResult encodeData(String data) {
        String lzwCompressed = lzwAlgorithm.compress(data);
        String gzipCompressed = gzipAlgorithm.compress(data);

        //#1 - lzw < data, gzip
        if (lzwCompressed.length() <= data.length() && lzwCompressed.length() <= gzipCompressed.length()) {
            return new CompressionResult(lzwCompressed, AlgorithmType.LZW);
        }

        //#2 - lz78 < data, gzip
        if (gzipCompressed.length() <= data.length()) {
            return new CompressionResult(gzipCompressed, AlgorithmType.GZIP);
        }

        //#3 - data < lzw, gzip
        return new CompressionResult(data, AlgorithmType.NONE);
    }

    /**
     * A function to decode a given data using the given algorithm type
     *
     * @param data - the data to decrypt
     * @param algorithmType - the algorithm type
     * @return - the decryption result
     */
    public String decodeData(String data, AlgorithmType algorithmType) {
        switch (algorithmType) {
            case LZW:
                lzwAlgorithm = LZWAlgorithm.getInstance();
                return lzwAlgorithm.decompress(data);
            case GZIP:
                gzipAlgorithm = GZipAlgorithm.getInstance();
                return gzipAlgorithm.decompress(data);
            case NONE:
            default:
                return data;
        }
    }
}
